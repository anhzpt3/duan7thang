<?php
namespace App\Http\Services;

use App\calendar;
use Illuminate\Support\Facades\DB;

class CalendarService{
    function __construct(calendar $calendar){
        $this->calendar = $calendar;
    }

    public function showList($kw,$showMore){
        if(!$kw || empty($kw)){
            if($showMore > 0){
            return $this->calendar->where('is_delete','=',0)
                ->orderBy('id','desc')
                ->paginate($showMore);
            }else{
            return $this->calendar->where('is_delete','=',0)
                ->orderBy('id','desc')
                ->paginate(15);
            }
		}else{
            $data =  $this->calendar->where([['day', $kw],
                                ['is_delete', '=', '0'],])
                            ->orderBy('id','desc')
                            ->paginate(15);
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function find($id){
		return $this->calendar->find($id);
    }

    public function getAll(){
        return $this->calendar->where('is_delete','=',0)->get();
    }

    public function storage($data){
        return $data->save();
    }

    public function delete($id){
        $data = $this->calendar->find($id);
        $data->status = -1;
        $data->save();
    }
    public function checkExists($id){
        return $this->calendar->where([['id', '=', $id],['is_delete', '=', '0'],])
        ->exists();
    }
}
?>
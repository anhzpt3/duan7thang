<?php
namespace App\Http\Services;

use App\Models\order_list;
use Illuminate\Support\Facades\DB;

class OrderService{
    function __construct(order_list $order_list){
        $this->order_list = $order_list;
    }

    public function show_calendar($kw){
        if(!$kw || empty($kw)){
              return $this->order_list->where([['status', 1],['is_delete','0'],])->orderBy('id','desc')
                                      ->orWhere([['is_delete', '=', '0'],['status', 2],])
                                      ->get();
            }else{
              $data =  $this->order_list->where([['name', 'like', "%$kw%"],['is_delete', '=', '0'],['status', 1],])
                                        ->orWhere([['phone', 'like', "%$kw%"],['is_delete', '=', '0'],['status', 1],])
                                        ->orderBy('id','desc')
                                        ->get();
              return $data;
        }
    }
    public function getAll(){
        return $this->order_list->where('is_delete','=',0)->get();
    }

    public function find($id){
		    return $this->order_list->find($id);
    }

    public function store($data){
      return $data->save();
    }
    
   
}
?>
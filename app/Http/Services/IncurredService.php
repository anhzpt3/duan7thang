<?php
namespace App\Http\Services;

use App\Models\incurred;
use Illuminate\Support\Facades\DB;

class IncurredService{
    function __construct(incurred $incurred){
        $this->incurred = $incurred;
    }

    public function store($data){
        return $data->save();
      }

}
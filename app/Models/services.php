<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class services extends Model
{
    protected $table ='services';
    protected $fillable = ['name','image','price','short_desc','content','status','is_delete'];
}

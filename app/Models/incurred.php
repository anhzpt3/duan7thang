<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class incurred extends Model
{
    protected $table ='incurred';
    protected $fillable = ['histories_id','name','price','is_delete'];
}

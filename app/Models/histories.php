<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class histories extends Model
{
    protected $table ='histories';
    protected $fillable = ['customer_id','order_list_id','message','is_delete'];
}

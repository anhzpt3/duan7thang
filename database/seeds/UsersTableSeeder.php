<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
    		[
	    		"email" => "admin@gmail.com",
                "password" => Hash::make('123456'),
                "role" => "100",
                "image_user" => "x"
	    	]
    	];
      	DB::table('users')->insert($users);
    }
}

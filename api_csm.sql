-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 10, 2020 lúc 11:07 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `api_csm`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `clinic_schedules`
--

CREATE TABLE `clinic_schedules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `day` date DEFAULT NULL,
  `service_id` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Lịch khám bệnh';

--
-- Đang đổ dữ liệu cho bảng `clinic_schedules`
--

INSERT INTO `clinic_schedules` (`id`, `name`, `day`, `service_id`, `created_at`, `updated_at`) VALUES
(1, '07/12', '2020-12-07', 14, '2020-12-06 23:42:55', '2020-12-06 23:42:56'),
(2, '08/12', '2020-12-08', 15, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(3, '09/12', '2020-12-09', 16, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(11, '10/12', '2020-12-10', 14, '2020-12-06 23:42:55', '2020-12-06 23:42:56'),
(12, '11/12', '2020-12-11', 15, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(13, '12/12', '2020-12-12', 16, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(14, '13/12', '2020-12-13', 14, '2020-12-06 23:42:55', '2020-12-06 23:42:56'),
(15, '14/12', '2020-12-14', 15, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(16, '15/12', '2020-12-15', 16, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(17, '08/12', '2020-12-08', 14, '2020-12-06 23:42:55', '2020-12-06 23:42:56'),
(18, '09/12', '2020-12-09', 15, '2020-12-06 23:45:44', '2020-12-06 23:45:44'),
(19, '10/12', '2020-12-10', 16, '2020-12-06 23:45:44', '2020-12-06 23:45:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `clinic_schedule_timeclass`
--

CREATE TABLE `clinic_schedule_timeclass` (
  `id` int(11) NOT NULL,
  `clinic_schedule_id` int(11) DEFAULT NULL,
  `timeclass_id` int(11) DEFAULT NULL,
  `is_booking` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `clinic_schedule_timeclass`
--

INSERT INTO `clinic_schedule_timeclass` (`id`, `clinic_schedule_id`, `timeclass_id`, `is_booking`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, '2020-12-06 23:50:51', '2020-12-08 23:39:41'),
(2, 1, 2, 0, '2020-12-06 23:51:05', '2020-12-08 23:08:10'),
(3, 1, 3, 0, '2020-12-06 23:51:05', '2020-12-08 23:09:33'),
(4, 3, 1, 1, '2020-12-08 07:25:27', '2020-12-09 01:12:46'),
(5, 3, 2, 0, NULL, '2020-12-08 02:16:51'),
(6, 3, 3, 1, NULL, '2020-12-09 01:11:19'),
(7, 4, 1, 0, NULL, '2020-12-08 01:14:10'),
(8, 4, 2, 0, NULL, NULL),
(9, 4, 3, 0, NULL, NULL),
(14, 11, 1, 0, NULL, '2020-12-08 01:14:10'),
(15, 12, 2, 0, NULL, NULL),
(16, 13, 3, 0, NULL, NULL),
(17, 11, 2, 0, NULL, '2020-12-08 01:14:10'),
(18, 11, 3, 0, NULL, '2020-12-08 01:14:10'),
(19, 12, 1, 0, NULL, NULL),
(20, 12, 3, 0, NULL, NULL),
(21, 13, 1, 1, NULL, '2020-12-10 02:18:16'),
(22, 13, 2, 0, NULL, NULL),
(23, 14, 3, 0, NULL, NULL),
(24, 14, 1, 0, NULL, NULL),
(25, 14, 2, 0, NULL, NULL),
(26, 15, 3, 0, NULL, NULL),
(27, 15, 1, 0, NULL, NULL),
(28, 15, 2, 0, NULL, NULL),
(29, 16, 3, 0, NULL, NULL),
(30, 16, 1, 0, NULL, NULL),
(31, 16, 2, 0, NULL, NULL),
(32, 17, 3, 0, NULL, NULL),
(33, 17, 1, 0, NULL, NULL),
(34, 17, 2, 0, NULL, NULL),
(35, 18, 3, 0, NULL, NULL),
(36, 18, 1, 0, NULL, NULL),
(37, 18, 2, 0, NULL, NULL),
(38, 19, 3, 0, NULL, NULL),
(39, 19, 1, 0, NULL, NULL),
(40, 19, 2, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `cmt` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `phone`, `date`, `cmt`, `user_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'nguyen huu van', 'duyvan@gmail.com', '0978894832', '2020-06-06', '123212343', 0, '1', '2020-12-07 19:41:08', '2020-12-08 23:42:18'),
(2, 'tuyennqph06237@fpt.edu.vn', 'admin@a.a', '0397872940', '2020-01-04', '123456765', 0, '0', '2020-12-07 20:24:10', '2020-12-07 20:24:10'),
(3, 'adfasdfasdf', 'tranhai9447@gmail.com', '3563456345634', '1969-02-02', '00221515', 0, '0', '2020-12-08 00:28:18', '2020-12-08 00:28:18'),
(4, 'quang minh', 'admin@222222a.a', '12121212', '2020-07-09', '1234324', 0, '0', '2020-12-08 01:05:54', '2020-12-08 01:05:54'),
(5, 'ádasdasd', 'ssssadmin@a.a', 'ádasdasdasd', '2020-07-02', '3423423423423', 0, '0', '2020-12-08 01:14:10', '2020-12-08 01:14:10'),
(6, 'nguyen quang tuyen 2', 'adminwewe@a.a', '43433434', '2020-07-09', '1224242', 0, '0', '2020-12-08 02:16:51', '2020-12-08 02:16:51'),
(7, 'nguyen quang tuyen', 'tuyennguyencp99@gmail.com', '0989998989', '2020-09-01', '12928321', 0, '1', '2020-12-08 02:31:27', '2020-12-08 02:49:03'),
(8, 'hoang the anh', 'anh\\', '0397822946', NULL, '1020202', 54, '0', '2020-12-08 02:50:05', '2020-12-08 02:50:05'),
(9, 'the anh', 'theanh@gmail.com', '123443', NULL, '121221', 59, '0', '2020-12-08 02:50:51', '2020-12-08 02:50:51'),
(10, 'the anh demo', 'th@gmail.com', '0909090909', '2020-12-05', '22222222', 60, '0', '2020-12-08 02:53:27', '2020-12-08 02:53:27'),
(11, 'tuyen demo', 'dsdas@gmail.com', '23873828', '2020-12-13', '22222222', 61, '0', '2020-12-08 02:58:08', '2020-12-08 02:58:08'),
(12, 'nguyen quang tuyen', 'adádamin@a.a', '094434334', '2020-04-02', '111111', 0, '1', '2020-12-08 03:19:40', '2020-12-08 23:41:50'),
(13, 'eqweqweqwe', 'thhest@gmail.com', '0397874946', '2020-01-01', '213123123', 0, '1', '2020-12-08 23:06:48', '2020-12-08 23:41:53'),
(14, 'tuyen dzai', 'ad1111min@a.a', '12121212', '2020-01-01', '12321212', 0, '0', '2020-12-08 23:08:10', '2020-12-08 23:08:10'),
(15, 'tuyen dep trai1', 'adm111in@a.a', '231313', '2020-03-03', '121212', 0, '1', '2020-12-08 23:09:33', '2020-12-08 23:59:05'),
(16, 'hoang anh', 'hoajg@gmail.com', '23423423', '2020-07-09', '12121212', 0, '1', '2020-12-08 23:39:41', '2020-12-08 23:43:01'),
(17, 'tuyennqph06237@fpt.edu.vn', 'tuyennguyencp99@gmail.com', '0397872123', '2020-01-08', '2323', 0, '0', '2020-12-09 01:11:19', '2020-12-09 01:11:19'),
(18, 'tao là tuyến', 'taolatuyen@gmail.com', '0967793300', '2020-01-08', '2323', 0, '1', '2020-12-09 01:12:46', '2020-12-09 02:13:16'),
(19, 'Hoàng Anh', 'anhzpt3@gmail.com', '0339315877', '1999-11-24', '12121212', 0, '0', '2020-12-10 02:18:16', '2020-12-10 02:18:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doctor`
--

CREATE TABLE `doctor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'images/images.png	',
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `doctor`
--

INSERT INTO `doctor` (`id`, `name`, `email`, `phone`, `info`, `image`, `user_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 'Đỗ Quang Minh 2', 'minhminh2@gmail.com', '0333333332', 'đây là bác sĩ', 'images/images.png	', '34', '0', '2020-11-18 05:05:38', '2020-11-18 05:15:42'),
(3, 'Đỗ Quang Minh', 'minhminh1111@gmail.com', '03333333332', 'đây là bác sĩ', 'images/doctor/5fb5102f78b7b-IMG20170810143343_1.jpg', '35', '0', '2020-11-18 05:14:39', '2020-11-18 05:21:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `histories`
--

CREATE TABLE `histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_list_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `histories`
--

INSERT INTO `histories` (`id`, `customer_id`, `order_list_id`, `message`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, '14', '9', 'đây là nội dung khám', '0', '2020-12-09 07:07:02', '2020-12-09 07:07:02'),
(2, '12', '8', 'đây là nội dung khám 2', '0', '2020-12-09 07:08:28', '2020-12-09 07:08:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `histories_login_ad`
--

CREATE TABLE `histories_login_ad` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `histories_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `incurred`
--

CREATE TABLE `incurred` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `histories_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `incurred`
--

INSERT INTO `incurred` (`id`, `histories_id`, `name`, `price`, `is_delete`, `created_at`, `updated_at`) VALUES
(8, '1', 'Chi phí 1', '120000', '0', '2020-12-09 07:07:02', '2020-12-09 07:07:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_11_080611_create_posts_table', 1),
(4, '2019_07_11_081637_alter_table_posts_add_column_status', 1),
(5, '2019_07_24_032234_create_categories_table', 1),
(6, '2019_07_24_032353_alter_table_posts_add_cate_id_column', 1),
(7, '2019_08_06_075253_alter_table_users_add_column_role_id', 1),
(8, '2019_08_09_082001_create_products_table', 1),
(9, '2019_08_09_082120_alter_table_products_add_cate_id_column', 1),
(10, '2019_08_22_045601_alter_table_products_add_price_column', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `day` date DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `clinic_schedule_id` int(11) NOT NULL,
  `message` text NOT NULL DEFAULT 'notthing',
  `is_delete` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order_list`
--

INSERT INTO `order_list` (`id`, `time_id`, `day`, `customer_id`, `clinic_schedule_id`, `message`, `is_delete`, `created_at`, `updated_at`, `status`) VALUES
(8, 3, NULL, 12, 3, 'notthing', 0, '2020-12-08 03:19:40', '2020-12-09 07:08:28', 2),
(9, 2, NULL, 14, 1, 'notthing', 0, '2020-12-08 23:08:10', '2020-12-09 07:07:02', 2),
(10, 3, NULL, 15, 1, 'notthing', 0, '2020-12-08 23:09:33', '2020-12-08 23:09:33', 0),
(11, 1, NULL, 16, 1, 'notthing', 0, '2020-12-08 23:39:41', '2020-12-08 23:39:41', 0),
(12, 3, NULL, 17, 3, 'notthing', 0, '2020-12-09 01:11:19', '2020-12-09 01:11:19', 0),
(13, 1, NULL, 18, 3, 'notthing', 0, '2020-12-09 01:12:46', '2020-12-09 01:12:46', 0),
(14, 1, NULL, 19, 13, 'notthing', 0, '2020-12-10 02:18:16', '2020-12-10 02:18:16', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `feature_image`, `content`, `status`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'Tại sao cần làm xét nghiệm nhóm máu ở bà bầu?', 'Tại-sao-cần-làm-xét-nghiệm-nhóm-máu-ở-bà-bầu?', 'images/posts/2.jpg', 'Khi xét nghiệm nhóm máu ở bà bầu cho kết quả là Rh(-) ở người mẹ, người cha có Rh(+) thì con của cặp vợ chồng này có thể gặp vấn đề về sức khỏe.\r\n\r\nNếu không có những bất thường khác, sự bất ', '1', '0', NULL, NULL),
(122, 'Bà bầu có uống được C sủi không? Lợi ích khi bổ sung vitamin C cho bà bầu', 'ba-bau-co-uong-duoc-c-sui-khong-loi-ich-khi-bo-sung-vitamin-c-cho-ba-bau', 'images/posts/3.jpg', 'Cả mẹ và bé đều cần vitamin C hàng ngày vì nó cần thiết để cơ thể tạo ra collagen, một loại protein cấu trúc là thành phần của sụn, gân, xương và da. Dựa trên các nghiên cứu trên động vật, một số nhà nghiên cứu tin rằng sự thiếu hụt vitamin C ở trẻ sơ sinh có thể làm giảm sự phát triển trí não.', '1', '0', '2020-12-03 02:28:38', '2020-12-03 02:28:52'),
(123, 'Bà bầu có uống được C sủi không? Lợi ích khi bổ sung vitamin C cho bà bầu', 'ba-bau-co-uong-duoc-c-sui-khong-loi-ich-khi-bo-sung-vitamin-c-cho-ba-bau', 'images/posts/4.jpg', 'Cả mẹ và bé đều cần vitamin C hàng ngày vì nó cần thiết để cơ thể tạo ra collagen, một loại protein cấu trúc là thành phần của sụn, gân, xương và da. Dựa trên các nghiên cứu trên động vật, một số nhà nghiên cứu tin rằng sự thiếu hụt vitamin C ở trẻ sơ sinh có thể làm giảm sự phát triển trí não.', '1', '0', '2020-12-03 02:28:38', '2020-12-03 02:28:50'),
(124, 'Bà bầu béo phì ảnh hưởng trí thông minh con trai', 'ba-bau-beo-phi-anh-huong-tri-thong-minh-con-trai', 'images/posts/5.jpg', 'Mẹ béo phì trong thai kỳ, bé trai sinh ra sẽ vận động kém hơn trẻ khác lúc 3 tuổi, IQ cũng thấp hơn 5 điểm khi lên 7.   Đây là kết quả nghiên cứu của các nhà khoa học Đại học Texas và Đại học Columbia sau khi khảo sát 368 bà mẹ và con của họ. Những phụ nữ này được yêu cầu báo cáo cân nặng trong ba tháng thứ hai và ba tháng cuối thai kỳ. Kỹ năng vận động và chỉ số IQ của những đứa trẻ năm ba và 7 tuổi cũng được kiểm tra kỹ lưỡng.   \"Điều đặc biệt là ngay cả khi sử dụng các đánh giá phát triển phù hợp với lứa tuổi khác nhau, mối liên hệ này vẫn tồn tại ở thời thơ ấu và trung niên, tức những tác động không biến mất theo thời gian\", theo Elizabeth Widen, tác giả nghiên cứu.    Kết quả vẫn đúng sau khi xét yếu tố như trình độ giáo dục người mẹ, trẻ có bị sinh non hay không. Tuy nhiên, môi trường giáo dục trẻ ở nhà - như tương tác với cha mẹ, trẻ có được tặng sách, đồ chơi hay không - có thể giảm tác động tiêu cực của bệnh béo phì ở mẹ bầu tới con trai. \"Tuy nhiên, ảnh hưởng này tới IQ trẻ thấp hơn so với khả năng vận động\", Elizabeth bổ sung.  Kết quả nghiên cứu mới được công bố trên BMC Pediatrics.', '1', '0', '2020-12-03 02:33:52', '2020-12-03 02:33:52'),
(125, 'Vitamin và khoáng chất cần thiết cho bà bầu', 'vitamin-va-khoang-chat-can-thiet-cho-ba-bau', 'images/posts/6.jpg', 'trong một ngày để thai nhi phát triển, hạn chế nguy cơ sẩy thai, sinh non.   Mẹ bầu cần cung cấp đầy đủ vitamin và khoáng chất mỗi ngày, ít nhất 10 loại. Chế độ dinh dưỡng cân đối sẽ giảm nguy cơ thiếu axit folic (vitamin B9) - một thành phần tham gia vào quá trình tạo máu; hạn chế thiếu máu do thiếu sắt. Nếu vi chất dinh dưỡng không đủ, nhất là kẽm sẽ tác động xấu đến sự phát triển và chức năng của hầu hết các tế bào miễn dịch, tế bào T...  Dưới đây là một số vitamin và khoáng chất mẹ bầu cần bổ sung trước và trong khi mang thai.  Canxi  Canxi cần thiết cho quá trình tạo xương của thai nhi, chức năng thần kinh và sự đông máu. Nếu mẹ bầu không bổ sung đủ canxi trong thai kỳ sẽ ảnh hưởng phát triển chiều cao của bé, tăng nguy cơ còi xương, mẹ bầu có khả năng bị tiền sản giật, loãng xương, hư răng... Phụ nữ có thai cần 1.200 mg canxi mỗi ngày, bà mẹ cho con bú là 1.300 mg; cao hơn mức bình thường.  Khẩu phần ăn của mẹ bầu cần nhiều thực phẩm chứa canxi như hải sản (tôm, cua, sò...), bơ, cải xanh, sữa và các sản phẩm từ sữa. Tép ăn cả vỏ, cá nhỏ để nhừ xương có chứa nhiều canxi có lợi cho mẹ bầu. Sữa nên uống hàng ngày. Phụ nữ mang thai có thể bổ sung canxi qua đường uống theo chỉ dẫn của bác sĩ.  Sắt  Sắt tham gia tạo huyết cầu tố (hemoglobin), tạo yếu tố miễn dịch, hô hấp tế bào và hỗ trợ khả năng nhận thức của con người. Nếu thiết sắt dễ dẫn đến thiếu máu, bào thai chậm phát triển, sinh non, tăng tỷ lệ tỷ vong của mẹ và con. Biểu hiện thiếu sắt thời kỳ đầu là thường mệt mỏi, giảm trí nhớ, da kém hồng hào; nặng hơn có thể rụng tóc, khó thở khi gắng sức, tim đập nhanh...  Thời điểm tốt nhất để bổ sung sắt là 3 tháng trước mang thai. Theo khuyến cáo của Tổ chức Y tế Thế giới (WHO), mỗi ngày thai phụ cần bổ sung 30-60 mg sắt mỗi ngày. Mẹ bầu cho con bú hoàn toàn trong 6 tháng đầu, có thể bổ sung sắt giống khi mang thai.  Bà bầu được khuyến khích ăn nhiều loại thực phẩm chứa sắt như thịt bò, thịt lợn, cá thu, trứng, đậu, rau xanh... Nếu uống viên sắt cần cách xa thời điểm uống bổ sung canxi vì sắt rất khó hấp thu và canxi làm cản trở quá trình này; không uống cùng trà, cà phê. Mẹ bầu nên uống sắt khi đói, uống cùng với vitamin C để tăng khả năng hấp thu.', '1', '0', '2020-12-03 02:35:08', '2020-12-03 02:35:41'),
(126, 'Bà bầu không nên ăn khoai tây chiên', 'ba-bau-khong-nen-an-khoai-tay-chien', 'images/posts/3.jpg', 'Thai phụ ăn nhiều khoai tây chiên có thể tăng lượng axit linoleic, gây hại cho bản thân và em bé trong bụng.  Axit linoleic là axit béo omega 6 có trong dầu thực vật và khoai tây chiên. Mỗi ngày, cơ thể chỉ cần một lượng rất nhỏ axit linoleic. Hàm lượng axit linoleic quá cao có thể gây những triệu chứng viêm và nguy cơ bệnh tim, nhất là đối với phụ nữ mang thai.  Khoai tây chiên chứa nhiều axit linoleic nên không tốt .  Các nhà khoa học đã thí nghiệm trên chuột mẹ đang mang thai và phát hiện những thay đổi tiêu cực khi chúng nạp quá nhiều axit linoleic trong 10 tuần. Cụ thể, protein gây viêm trong gan và protein làm tăng co bóp tử cung, hormone tăng trưởng giảm.   Từ kết quả trên, các nhà khoa học khuyến cáo phụ nữ mang thai hạn chế ăn khoai tây chiên để không làm tăng axit linoleic, kiểm soát lượng đường và muối vào cơ thể.', '1', '0', '2020-12-03 02:36:52', '2020-12-03 02:37:45'),
(127, 'Thói quen xấu mẹ bầu tưởng tốt', 'thoi-quen-xau-me-bau-tuong-tot', 'images/posts/4.jpg', 'Thai phụ không nên đi bộ quá mức, giảm cà phê, hạn chế tiếp xúc các thiết bị điện tử và wifi.  Để thai nhi được phát triển tốt nhất, mẹ bầu nên từ bỏ những thói quen không tốt.  Không nên đi bộ quá mức  Đi bộ là một bài tập an toàn với nhiều lợi ích như cải thiện sức khỏe tim mạch và mang lại vóc dáng đẹp, giảm nguy cơ mắc bệnh tiểu đường thai kỳ và tiền sản giật, giảm căng thẳng, hạn chế mất ngủ, giúp co giãn cơ vùng bụng, khung xương chậu tốt để chuyển dạ dễ dàng hơn.  Thai phụ không nên lạm dụng đi bộ quá nhiều mà cần \"đi đúng - đi đủ\".  - Đi đúng  Giữ thẳng người sao cho trọng lượng cơ thể dồn đều trên hai chân. Mắt nhìn thẳng về phía trước, cằm giữ thẳng, song song với mặt đất, toàn thân thư giãn, không đi bộ quá nhanh, đi nhẹ nhàng và chắc chắn.  Không nên đi bộ trong điều kiện khắc nghiệt, trời quá nóng hoặc mưa to, khói bụi nhiều.  Giai đoạn 3 tháng đầu thai kỳ hãy lựa chọn cho mình loại giày thấp, vừa chân, cổ giày cao vừa đủ ôm, không đi nhanh.  Giai đoạn 3 tháng tiếp theo điều chỉnh tư thế mỗi lần đi bộ sao cho hông chuyển động chậm hơn, cẩn thận khi xoay người và giữ dáng người thẳng.  Giai đoạn 3 tháng cuối nên đi bộ cùng người thân, chọn những địa điểm gần nhà, địa hình dễ di chuyển.  - Đi đủ  Nếu trước khi mang thai mẹ bầu đã thường xuyên đi bộ thì hãy tiếp tục thói quen này. Tuy nhiên chỉ nên đi bộ với khoảng thời gian tối đa một giờ và hãy chia thành nhiều lần trong ngày.  Nếu chưa đi bộ trước đi mang thai, các mẹ bầu nên bắt đầu bằng việc đi dạo chậm, 15-30 phút mỗi ngày, một tuần đi 3 lần. Nếu không tham gia các môn thể thao khác trong quá trình mang thai, nên đi bộ 2 lần mỗi ngày.  Lưu ý trong những tháng đầu của thai kỳ, không nên đi bộ quá nhiều vì sẽ có thể dẫn đến tình trạng tử cung gò nhiều, gây những trường hợp không mong muốn như sảy thai và sinh non.', '1', '0', '2020-12-03 02:40:55', '2020-12-03 02:40:55'),
(128, 'Thực phẩm dễ ảnh hưởng đến thai nhi', 'thuc-pham-de-anh-huong-den-thai-nhi', 'images/posts/5.jpg', 'Đu đủ, rau bồ ngót, gan động vật, thịt tái sống... bà bầu nên hạn chế ăn.  Chuyên gia dinh dưỡng Nguyễn Mộc Lan cho biết nhiều thực phẩm tốt cho sức khỏe nhưng không thích hợp với bà bầu.   Đu đủ, dứa  Đu đủ có tác dụng kích thích tuyến sữa và cung cấp nhiều đạm cho phụ nữ sau sinh. Tuy nhiên với bà bầu, đu đủ xanh chứa chất papain hoạt động giống như hormone prostaglandin và oxytocin gây co thắt tử cung. Ăn đu đủ xanh thai phụ cũng có thể bị phù và xuất huyết, dẫn tới sinh non.  Dứa cũng có nguy cơ gây sảy thai do chứa bromelain làm mềm, co thắt tử cung. Bác sĩ thường khuyên thai phụ tránh ăn hoặc uống nước dứa.  Rau bồ ngót Rau ngót chứa chất papaverin gây ra hiện tượng giãn cơ trơn của tử cung, có thể dẫn đến sảy thai, tiêu chảy. Bà bầu ăn hơn 30 g lá rau ngót tươi có nguy cơ bị sảy thai. Nếu thai phụ có tiền sử sảy thai liên tục, sinh non hay hiếm muộn không nên ăn canh rau ngót, uống nước ép lá rau ngót chưa qua chế biến.  Ngải cứu  Ngải cứu có công dụng giảm nhức mỏi, lưu thông máu, giảm đau bụng.  Tuy nhiên, một số nghiên cứu cho thấy ăn ngải cứu trong 3 tháng đầu thai kỳ sẽ làm tăng nguy cơ tử cung chảy máu, co thắt, dẫn đến sảy thai hoặc sinh non. Vì vậy, nếu có ý định ăn ngải cứu để dưỡng thai, mẹ bầu nên tham khảo ý kiến bác sĩ chuyên khoa. Bà bầu có tiền sử sảy thai hoặc sinh non không nên ăn nhiều ngải cứu.  Rau má  Rau má có nhiều lợi ích về sức khỏe, thanh nhiệt cơ thể. Do tính lợi tiểu nên rau má giúp hạ huyết áp, điều trị ngộ độc sắn.  Rau má lại là một trong những thực phẩm được cảnh báo với bà bầu. Phụ nữ uống rau má khó có cơ hội thụ thai. Bà bầu uống nước rau má có thể dẫn đến sảy thai, đầy bụng.  Trà, cà phê  Trà và cà phê đều chứa caffeine, giúp cơ thể hưng phấn. Tuy nhiên, mức caffeine cao có thể làm tăng nguy cơ sảy thai ở bà bầu, thai nhi nhẹ cân, đẻ khó. Caffeine có thể tác động quá mức lên hệ tim mạch thai phụ.  Caffeine còn có trong chocolate, một số loại nước giải khát, nước uống tăng lực. Một số thuốc cũng chứa caffeine, các bà bầu cần lưu ý.  Gan động vật và ruột non  Gan động vật tồn dư nhiều chất độc có thể gây hại cho phụ nữ mang thai. Ruột non là bộ phận dễ nhiễm ký sinh trùng đường ruột.  Gan ruột cũng nhiều cholesterol, ảnh hưởng xấu đến thai nhi. Người bình thường cũng được các chuyên gia dinh dưỡng khuyến cáo hạn chế ăn gan, ruột động vật.  Một số loại cá  Cá nhiều đạm và axit béo omega 3 có lợi cho bà bầu. Tuy nhiên, một số loại cá ở tầng sâu có nguy cơ nhiễm kim loại thủy ngân như cá mập, cá ngừ đại dương, cá kiếm, cá thu, cá kình... thai phụ nên hạn chế ăn vì có thể ảnh hưởng đến não bộ thai nhi.  Món tái, sống  Bà bầu nên tránh hoàn toàn những món như sushi, tái, lòng đỏ trứng, bởi nếu chưa được chế biến kỹ chúng dễ nhiễm ký sinh trùng Toxoplasmosis, nguy cơ sảy thai, thai chết lưu...', '1', '0', '2020-12-03 02:42:50', '2020-12-03 02:42:50'),
(129, 'Bà bầu ăn đào sinh con có nhiều lông?', 'ba-bau-an-dao-sinh-con-co-nhieu-long', 'images/posts/6.jpg', 'Em mang thai ở tháng thứ 7, thích ăn đào, nghe nói mẹ bầu ăn đào sinh con sẽ có nhiều lông. Xin hỏi bác sĩ thực hư thế nào?  Trả lời:  Quan điểm bà bầu ăn đào con sinh ra sẽ lắm lông là hoàn toàn không cơ sở.  Đào là loại trái cây rất giàu dinh dưỡng và có thể được ăn khi mang thai. Vitamin C có trong đào giúp cho xương, răng, da, cơ và các mạch máu của em bé phát triển khỏe mạnh. Vitamin C cũng hỗ trợ quá trình hấp thu sắt, một vi chất rất quan trọng cần thiết trong suốt quá trình mang thai cho cả mẹ và bé.  Lượng folate có trong đào giúp ngăn chặn các dị tật về ống thần kinh như tật nứt đốt sống. Kali trong đào giúp làm giảm tình trạng co thắt cơ và giảm mệt mỏi thường gặp trong thai kỳ. Lượng chất xơ có trong đào có tác dụng hỗ trợ tiêu hóa và làm giảm táo bón, một vấn đề mà nhiều bà bầu rất lo ngại.  Tuy nhiên, đào có tính nóng nên nếu ăn nhiều, liên tục, bà bầu có thể bị xuất huyết. Hơn nữa, với những quả đào có lông, phần này bám ở vỏ có thể gây kích thích ngứa họng hoặc dị ứng nên tốt nhất là các mẹ bầu nên gọt vỏ khi ăn. Phụ nữ có thai cũng chỉ nên ăn mỗi tuần khoảng 2-3 quả để không gây hại gì cho mẹ và bé', '1', '1', '2020-12-03 02:45:28', '2020-12-08 03:04:46'),
(130, 'Bà bầu không nên chủ quan khi bị đau rát cổ họng', 'ba-bau-khong-nen-chu-quan-khi-bi-dau-rat-co-hong', 'images/posts/3.jpg', 'asdasd', '1', '1', '2020-12-03 02:47:16', '2020-12-07 22:13:48'),
(131, 'Hai món ngon bổ rẻ dành cho bà bầu', 'hai-mon-ngon-bo-re-danh-cho-ba-bau', NULL, 'Canh đu đủ giò heo, mực dồn thịt sốt cà chua thơm ngon bổ dưỡng, tốt cho phụ nữ mang thai.  Kỹ sư dinh dưỡng Trương Thị Nhàn cho biết trong quá trình mang thai và cho con bú, bà bầu cần thêm vào khẩu phần ăn uống mỗi ngày khoảng 350 kcal mỗi ngày. Khẩu phần này nhằm đảm bảo lượng dinh dưỡng cần thiết để nuôi thai nhi, đồng thời tạo đủ nguồn sữa mẹ cho con bú khi bé chào đời.  Canh đu đủ giò heo  Nguyên liệu: - Giò heo 500 g. - Đu đủ xanh hoặc chín hường 500 g. - Củ hành tím băm. - Hành ngò, tiêu, muối, nước mắm, dầu, đường, hạt nêm... mỗi thứ một ít. Cách làm: - Giò heo cạo rửa sạch, ướp muối, tiêu, củ hành băm. - Đu đủ gọt vỏ, cắt miếng vừa ăn. - Hành ngò lặt rửa sạch, cắt nhỏ. - Bắc nước đủ dùng lên bếp nấu vừa nóng, cho giò vào nấu lửa lớn, vớt bọt và cho thêm ít hạt nêm. - Cho đu đủ vào nấu lửa vừa. Khi đu đủ và giò chín mềm thì nêm nước mắm vừa ăn. - Múc canh ra tô, rắc hành ngò, tiêu lên trên.  Mực dồn thịt sốt cà  Nguyên liệu: - Mực ống 500 g. - Thịt nạc băm 200 g. - Cà chua 200 g. - Nấm mèo 5 tai. - Bún tàu một lọn nhỏ. - Tỏi, ớt, hành ngò. - Dầu ăn, muối, đường, nước tương… mỗi thứ một ít.', '1', '1', '2020-12-03 02:49:36', '2020-12-07 22:13:32'),
(132, 'Bà bầu nên làm gì khi chuyển dạ?', 'ba-bau-nen-lam-gi-khi-chuyen-da', NULL, 'Khi cơn co thắt bắt đầu, người mẹ nên thở ra nhè nhẹ, chậm rãi và tưởng tượng về những hình ảnh tươi đẹp sẽ giúp giảm cảm giác đau đớn, khó chịu.  Theo bác sĩ Trần Thị Minh Nguyệt, thông thường quá trình chuyển dạ kéo dài và những cơn đau quá mức có thể khiến bà bầu bị kiệt quệ, không còn đủ sức để rặn đẻ khiến đứa trẻ sinh ra dễ bị ngạt. Do vậy bác sĩ khuyên phụ nữ mang thai nên chủ động tìm hiểu rõ về cơ chế của cơn đau chuyển dạ, cách thức giảm đau sẽ giúp tăng khả năng chịu đựng và vượt qua giai đoạn này nhẹ nhàng hơn. Làm quen với cơn đau chuyển dạ  Tất cả cơn chuyển dạ đều gây đau đớn. Tùy vào cơ địa mỗi thai phụ mà mức độ đau và các cơn co thắt diễn ra khác nhau. Lúc này, thay vì lo lắng, hoảng sợ, người mẹ nên tự xây dựng cho mình lòng tin bằng cách chuẩn bị đối mặt với cường độ mạnh của các cơn co thắt, hiểu được khả năng chịu đựng của cơ thể và học cách làm giảm đau. Nên nhớ rằng cơn đau chính là một phần rất tích cực của sự chuyển dạ, vì cứ sau mỗi lần co thắt thì thời khắc chào đời của con càng đến gần hơn.  Thả lỏng cơ thể với các phương pháp thở  Thả lỏng và tập trung thở sẽ giúp thai phụ bớt lo âu và giảm đau. Có hai cách thở nên áp dụng trong lúc này: thở chầm chậm và thở nhẹ nhàng. Trong các giai đoạn đầu tiên khi cơn co thắt bắt đầu nên thở ra nhè nhẹ và chậm rãi qua miệng sau đó hít vào từ từ qua mũi. Cứ giữ cách đó đều đều suốt cơn co thắt thường kéo dài từ 4 đến 6 giây. Đến khi cơn co thắt trở nên mạnh và thường xuyên hơn, người mẹ nhận thấy dễ thở thì bắt đầu áp dụng cách thở nhẹ, ngắn. Lưu ý: Khi thở, chỉ dùng phần trên của cơ thể, tránh dùng phần bụng dưới, nơi các cơn co thắt đang diễn ra dồn dập.  Tư thế khi chuyển dạ  Nhiều tư thế sẽ hỗ trợ tích cực cho bạn vượt cạn dễ dàng hơn. Có thể đi qua đi lại, dựa vào tường và lắc lư vùng chậu để sức nặng của bé trong bụng dồn về trước giúp giảm lực đè lên xương sống, tăng hiệu quả các cơn co thắt. Hoặc ngồi trên ghế, ngả người ra trước, hai chân dang ra. Cũng có thể bò nhằm giảm đau lưng khi cơn co thắt tiến triển mạnh hơn. Giữ cho 2 chân dang rộng, lưng thẳng, lắc vùng xương chậu. Nếu thấy mỏi có thể nằm hơi nghiêng, kê gối ở đầu và phần đùi trên, 2 chân dang ra, xuôi 2 tay, nhắm mắt thư giãn và tập trung thở.  Phát huy sức tưởng tượng  Mường tượng các hình ảnh tươi đẹp trong đầu giúp mẹ bầu giảm đau, giảm sợ hãi. Khi bắt đầu một cơn co thắt, hãy cố gắng tưởng tượng đến những hình ảnh giúp bạn dễ chịu và yêu thích, chẳng hạn như một bờ biển mát rượi hay đồng cỏ xanh ngát đầy hoa. Cơn co thắt đầu tiên xuất hiện khi cổ tử cung đang giãn nở, có thể hình dung ra hình ảnh một nụ hoa xinh xắn đang từ từ hé mở từng cánh một, sẽ rất có ích cho bạn. Cũng có thể nghĩ đến các con sóng vỗ bờ từng đợt tương ứng với từng cơn co tử cung. Thực tế nhiều thai phụ cho biết họ cảm thấy rất dễ chịu khi áp dụng cách này để giảm đau.  Nghe nhạc cũng là cách rất hiệu quả để giảm đau khi vượt cạn. Một bản nhạc du dương trầm bổng sẽ giúp thai phụ vượt lên các cơn co thắt. Những bài hát nhịp điệu mạnh dần sẽ giúp bạn tăng sức chịu đựng để đương đầu với các cơn co thắt mạnh hơn.  Can thiệp y khoa  Khi các cơn đau vượt quá ngưỡng chịu đựng hoặc thai phụ lo sợ đau quá sẽ không còn đủ sức vượt cạn, có thể cân nhắc đến các phương pháp can thiệp y khoa như thuốc giảm đau, thuốc gây tê, thuốc an thần. Sử dụng các loại thuốc an thần với liều lượng ít làm dịu các cơn đau, giảm lo âu, giúp thai phụ nghỉ ngơi giữa các cơn co thắt đồng thời kiềm chế triệu chứng nôn mửa và tăng huyết áp. Tuy nhiên thuốc an thần lại gây cảm giác buồn ngủ nên cần được bác sĩ tư vấn cặn kẽ trước khi quyết định sử dụng.  Lưu ý: Nhiều bà bầu không thích dùng thuốc tê khi chuyển dạ vì e ngại những tác dụng phụ có thể xảy ra như say thuốc, chóng mặt, nôn mửa, ức chế hô hấp và say thuốc ở bé. Tuy nhiên trong trường hợp cơn đau đã vượt quá ngưỡng chịu đựng, chuyển dạ kéo dài gây kiệt sức thì việc giảm đau bằng thuốc là cần thiết. Để biết phương pháp nào phù hợp nhất với mỗi người, trước đó nên nhờ bác sĩ hoặc hộ sinh tư vấn về các phương pháp giảm đau có sẵn ở bệnh viện cũng như tác dụng và phản ứng phụ có thể gặp phải. Như thế, thai phụ quyết định lựa chọn phương pháp nào khi quá trình chuyển dạ gặp khó khăn.', '1', '1', '2020-12-03 02:52:05', '2020-12-07 22:13:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `short_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `price`, `short_desc`, `content`, `is_delete`, `created_at`, `updated_at`) VALUES
(14, 'Siêu âm thai 3d', 'images/service/2.jpg', 1, 'tieu de 1', 'Siêu âm 3D là hình thức siêu âm cho thấy những hình ảnh 3 chiều về những chuyển động đang xảy ra bên trong cơ thể mẹ và bé. Như mẹ bầu đã biết ở siêu âm 2D hình ảnh siêu âm chỉ có màu đen và trắng, thì ở siêu âm 3D, đó là những hình màu rất rõ ràng và sắc nét. \r\n\r\nVào những tháng cuối của thai kỳ, khi thai nhi gần như đã phát triển đầy đủ về cấu trúc. Lúc đó, tiến hành siêu âm 3D có thể nhìn thấy tương đối rõ hình hài từ đường chân tay cho đến những đường nét mỏng manh trên khuôn mặt của con, hình ảnh này tương đối giống so với con khi mới vừa sinh ra. ', '0', '2020-11-20 01:53:17', '2020-11-20 01:53:33'),
(15, 'Siêu âm thai 2d', 'images/service/3.jpg', 111111, 'ádasd', 'Siêu âm thai 3D. Siêu âm thai 5D là kỹ thuật hiện đại, nâng cấp nhất hiện nay. Kết hợp giữa phương pháp siêu âm 4D và siêu âm màu Doppler, siêu âm thai 5D cho chất lượng hình ảnh rõ nét, chân thực nhất về hình thái, cử chỉ, biểu cảm của thai nhi. Đồng thời, đây cũng là loại siêu âm giúp phát hiện sớm các dị tật của thai nhi ngay từ tam cá nguyệt đầu của thai kỳ.', '0', '2020-11-20 02:19:55', '2020-12-06 20:33:19'),
(16, 'Siêu âm màu', 'images/service/4.jpg', 111111, 'ádasd', 'Siêu âm thai 2D. Siêu âm thai 5D là kỹ thuật hiện đại, nâng cấp nhất hiện nay. Kết hợp giữa phương pháp siêu âm 4D và siêu âm màu Doppler, siêu âm thai 5D cho chất lượng hình ảnh rõ nét, chân thực nhất về hình thái, cử chỉ, biểu cảm của thai nhi. Đồng thời, đây cũng là loại siêu âm giúp phát hiện sớm các dị tật của thai nhi ngay từ tam cá nguyệt đầu của thai kỳ.', '0', '2020-11-20 02:19:56', '2020-12-06 20:33:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service_history`
--

CREATE TABLE `service_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `histoty_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting`
--

CREATE TABLE `setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_footer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maps` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `timeclass`
--

CREATE TABLE `timeclass` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `timeclass`
--

INSERT INTO `timeclass` (`id`, `name`, `time_start`, `time_end`, `message`, `created_at`, `updated_at`) VALUES
(1, 'ca 10h-10h30', '7', '7.30', NULL, NULL, '2020-12-05 04:10:14'),
(2, 'ca 9h0h-9h30', '7.30', '8', NULL, NULL, '2020-12-03 00:32:47'),
(3, 'ca 8h-8h30', '8', '8.30', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `phone`, `email_verified_at`, `password`, `role`, `is_delete`, `remember_token`, `created_at`, `updated_at`) VALUES
(30, 'admin@gmail.com', '866238', NULL, '$2y$10$IxHDWTo/ZSfzNkQ.ketmGuV1igcGvjBN5bwyjIVGkv5udsdPahRWC', '100', '0', NULL, NULL, NULL),
(31, 'anh@gmail.com', '12345678', NULL, '$2y$10$t/zv0pwc5mpflzFD1gCp4uUszyOPzcfhVDLwA2CGKS7qnk5X4Jgfq', '100', '0', NULL, '2020-10-12 02:23:31', '2020-10-12 02:23:31'),
(33, 'tuyennguyencp99@gmail.com', '033877788', NULL, '$2y$10$STAenrES3otC9qQssC59p.JlEYzhD7JTobdAtbgK8l8XkLHBb7/b2', '1', '1', NULL, '2020-11-18 02:43:24', '2020-12-03 01:30:59'),
(34, 'minhminh2@gmail.com', '0333333332', NULL, '$2y$10$PDS6JjkEiPqiQEMf99gL9OGOlBHl61Unib7CWQtbHPY6TZfd/i9Sa', '100', '0', NULL, '2020-11-18 05:05:38', '2020-11-18 05:15:42'),
(35, 'minhminh1111@gmail.com', '03333333332', NULL, '$2y$10$6vQQNJ/SwkXtRwHHHPBTPeJjbWwalvIW1ZukTSoY4NiLhEsbOIDPm', '1', '1', NULL, '2020-11-18 05:14:39', '2020-11-18 05:21:47'),
(36, NULL, '03333333', NULL, '$2y$10$JSyjjdDSlq96QkRxEYtPY.irzLkq0HrBHTNeEJLF2Zeh8JQQG3WNC', '1', '1', NULL, '2020-11-21 03:00:40', '2020-12-03 01:30:44'),
(37, 'tuyenngussssyencp99@gmail.com', '0397872946', NULL, '$2y$10$/He6Ax3kSgMiNeq1NkRJ/eKDD7OVPC8LeL5MCWBxfbm8bDWPOu1C.', '1', '0', NULL, '2020-11-21 03:02:03', '2020-11-21 03:02:03'),
(39, 'tuyenngussssyencp99@gmail.com', '0397872942', NULL, '$2y$10$N6ApWSdJAaGbxBeGidpM8.ITn5gmt.5TAPfVfHGhjwZpLRWr/BGFS', '1', '0', NULL, '2020-11-21 03:02:23', '2020-11-21 03:02:23'),
(40, 'tuyenngussssyencp99@gmail.com', '0397872921', NULL, '$2y$10$6jj0Nry.Ae6rvTFyvB775.bd/y8tMtOALZNCLeB6lKhIufAmdQcDm', '1', '0', NULL, '2020-11-21 03:02:30', '2020-11-21 03:02:30'),
(42, 'tuyenngussssyencp99@gmail.com', '11111', NULL, '$2y$10$EDuLAZqzY3RMc5zzIkOmFO.8bFnRPf1y8.wDpSFkrSRoEBSQZG576', '1', '1', NULL, '2020-11-21 03:02:55', '2020-12-03 01:30:47'),
(44, 'ab@gmail.com.vn', '0397872943', NULL, '$2y$10$yBcYfuyE6YeJxZ9W40gC0uRgF91hHxQksejKfvookTFtRXH2rUmSO', '1', '1', NULL, '2020-12-01 06:24:53', '2020-12-03 01:30:48'),
(46, 'minh@gmail.com', '1234432', NULL, '$2y$10$JnDGBzqcd/BBMbc2zxQW3O5uh.YEEmX/mOuCDJXj9hwuPwxWVL.4m', '1', '1', NULL, '2020-12-02 21:29:56', '2020-12-03 01:30:50'),
(47, 'admsssin@a.a', '0397122946', NULL, '$2y$10$lo7reZEYAIgeh7tvaztL9.9ut1zTNH7ENKXiNN8NOHN77MI62OH/G', '1', '1', NULL, '2020-12-03 01:29:39', '2020-12-03 01:31:07'),
(52, '1admin@a.a', '1397872946', NULL, '$2y$10$AGBjZG4mbGQwC.xyAD9tHu7HFAuvA5TklElzQSuXP4/T7MfNjFSz2', '1', '0', NULL, '2020-12-03 01:33:47', '2020-12-03 01:33:47'),
(53, 'aádasddsmin@a.a', '0397272946', NULL, '$2y$10$GTmgnauDd2252iC7Zt.4Z.01ks88FQzPFIeedb5hooRQo5QGW7mGK', '1', '0', NULL, '2020-12-03 01:39:53', '2020-12-03 01:39:53'),
(54, 'anh\\', '0397822946', NULL, '$2y$10$A9qoSyqwAJGUSy3u95Nmy.i6sdIKE1VqDsx5NTLuyiOln3Up/bewi', '1', '0', NULL, '2020-12-08 02:50:05', '2020-12-08 02:50:05'),
(59, 'theanh@gmail.com', '123443', NULL, '$2y$10$.O84noQXhoL/xcMokfTd3.7lFlc/nqljbuetd7R6oPnyfNKEr3FnO', '1', '0', NULL, '2020-12-08 02:50:51', '2020-12-08 02:50:51'),
(60, 'th@gmail.com', '0909090909', NULL, '$2y$10$KMZoQQjunR4SJb2umbP19eSrU9LAlKV8BXpeazK/Re60WReWoFaKa', '1', '0', NULL, '2020-12-08 02:53:27', '2020-12-08 02:53:27'),
(61, 'dsdas@gmail.com', '23873828', NULL, '$2y$10$cu0jOjEHCzVYTDrsuMKv1e0ZfWQ/xW2ZulmtwE463DFIHyto3Bv5.', '1', '0', NULL, '2020-12-08 02:58:08', '2020-12-08 02:58:08'),
(65, 'admin@a.a', '0397872333', NULL, '$2y$10$N9Fvl5mG6QtgSzzcXlZYJOtyWfMS29d06CROHZxdW5mAEErXjNHNG', '1', '0', NULL, '2020-12-09 00:46:22', '2020-12-09 00:46:22'),
(66, 'ab@gmail.com.vn', '0397872111', NULL, '$2y$10$piE27dAwZ5UI.sOIbRDvkePtxlFdM5ljf/ZWR7mo1omf71aoD4wuW', '1', '0', NULL, '2020-12-09 00:47:25', '2020-12-09 00:47:25'),
(67, 'ab@gmail.com.vn', '1197872946', NULL, '$2y$10$s3vpXBUQk6SS3CcvpXLxienhSJMQ/RgLwPnYZAysveRsM0jVChgcq', '1', '0', NULL, '2020-12-09 01:04:30', '2020-12-09 01:04:30'),
(68, 'inpyzxe@gmail.com', '03978729422', NULL, '$2y$10$8YvxTSoYf7qKrj6.B9fw3ew0OAStahmv7W.8WgBTs2dxXzWO8nWVK', '1', '0', NULL, '2020-12-09 01:06:28', '2020-12-09 01:06:28'),
(69, 'tuyennguyencp99@gmail.com', '0397872123', NULL, '$2y$10$zqXj6sXeBHneULEBn4d1Hu/78vbWf6Ezx7eZNSX1mVlI9IOvQ4EQ2', '1', '0', NULL, '2020-12-09 01:11:19', '2020-12-09 01:11:19'),
(70, 'taolatuyen@gmail.com', '0967793300', NULL, '$2y$10$73Xe6sBWY9sILbJfzjT5Huaq78BrJnMpAF2ZkeLmr8XYEWNcDfixO', '1', '0', NULL, '2020-12-09 01:12:46', '2020-12-09 01:12:46'),
(71, 'anhzpt3@gmail.com', '0339315877', NULL, '$2y$10$FcPzMbhmGn.fn0VkvyfsxuDet/dsH1nlgoAJE5vjWmPmICJb.NwkO', '1', '0', NULL, '2020-12-10 02:18:16', '2020-12-10 02:18:16');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `clinic_schedules`
--
ALTER TABLE `clinic_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Chỉ mục cho bảng `clinic_schedule_timeclass`
--
ALTER TABLE `clinic_schedule_timeclass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_schedule_id` (`clinic_schedule_id`),
  ADD KEY `timeclass` (`timeclass_id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `histories_login_ad`
--
ALTER TABLE `histories_login_ad`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `incurred`
--
ALTER TABLE `incurred`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `service_history`
--
ALTER TABLE `service_history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `timeclass`
--
ALTER TABLE `timeclass`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `clinic_schedules`
--
ALTER TABLE `clinic_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `clinic_schedule_timeclass`
--
ALTER TABLE `clinic_schedule_timeclass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `incurred`
--
ALTER TABLE `incurred`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT cho bảng `timeclass`
--
ALTER TABLE `timeclass`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
